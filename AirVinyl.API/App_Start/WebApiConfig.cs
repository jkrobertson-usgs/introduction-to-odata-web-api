﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
//Added
using System.Web.OData.Extensions;
using Microsoft.OData.Edm;
using System.Web.OData.Builder;
using AirVinyl.Model;
using System.Web.OData.Batch;

namespace AirVinyl.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //By adding the Default OData Batch Handler you can now do batch commands through
            // url/odata/$batch  
            //See ExampleBatchRequest.txt in App_Start for example
            config.MapODataServiceRoute("ODataRoute", "odata", GetEdmModel()
                , new DefaultODataBatchHandler(GlobalConfiguration.DefaultServer));
            config.EnsureInitialized();
        }

        private static IEdmModel GetEdmModel()
        {
            var builder = new ODataConventionModelBuilder();
            builder.Namespace = "AirVinyl";
            builder.ContainerName = "AirVinylContainer";

            builder.EntitySet<Person>("People");
            builder.EntitySet<RecordStore>("RecordStores");
            //Because we set VinylRecords to be a Contained collection in people, it is now only accessible through the People controller. 
            //builder.EntitySet<VinylRecord>("VinylRecords");

            //Defines a function that can be called
            var isHighRatedFunction = builder.EntityType<RecordStore>().Function("IsHighRated");
            isHighRatedFunction.Returns<bool>();
            isHighRatedFunction.Parameter<int>("minimumRating");
            //Optional
            isHighRatedFunction.Namespace = "AirVinyl.Functions";

            //Create a new function that works on a set
            var areRatedByFunction = builder.EntityType<RecordStore>().Collection.Function("AreRatedBy");
            areRatedByFunction.ReturnsCollectionFromEntitySet<RecordStore>("RecordStores");
            areRatedByFunction.CollectionParameter<int>("personIds");
            areRatedByFunction.Namespace = "AirVinyl.Functions";

            //create root level unbound function (always add record type to be descriptive)
            var getHighRatedRecordStores = builder.Function("GetHighRatedRecordStores");
            getHighRatedRecordStores.Parameter<int>("minimumRating");
            getHighRatedRecordStores.ReturnsCollectionFromEntitySet<RecordStore>("RecordStores");
            getHighRatedRecordStores.Namespace = "AirVinyl.Functions";

            //Create an entity type action
            var rateAction = builder.EntityType<RecordStore>().Action("Rate");
            rateAction.Returns<bool>();
            rateAction.Parameter<int>("rating");
            rateAction.Parameter<int>("personId");
            rateAction.Namespace = "AirVinyl.Actions";

            //Create Entity Type Collection Action
            var removeRatingsAction = builder.EntityType<RecordStore>().Collection.Action("RemoveRatings");
            removeRatingsAction.Returns<bool>();
            removeRatingsAction.Parameter<int>("personId");
            removeRatingsAction.Namespace = "AirVinyl.Actions";

            // root level unbound actions (no return type)
            var removeRecordStoreRatingsAction = builder.Action("RemoveRecordStoreRatings");
            removeRatingsAction.Parameter<int>("personId");
            removeRatingsAction.Namespace = "AirVinyl.Actions";

            //Create Singleton
            builder.Singleton<Person>("Tim");

            return builder.GetEdmModel();
        }
    }
}
