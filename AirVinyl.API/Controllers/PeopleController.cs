﻿using AirVinyl.DataAccessLayer;
using AirVinyl.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;

namespace AirVinyl.API.Controllers
{
    public class PeopleController : ODataController
    {
        private AirVinylDbContext _ctx = new AirVinylDbContext();

        #region Person
        [EnableQuery( PageSize =  4)]
        public IHttpActionResult Get()
        {
            return Ok(_ctx.People);
        }
        
        [EnableQuery]
        public IHttpActionResult Get([FromODataUri]int key)
        {
            //var person = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            //if (person == null) return NotFound();
            //return Ok(person);

            var people = _ctx.People.Where(p => p.PersonId == key);
            if(!people.Any())
            {
                return NotFound();
            }
            //Creates an IQueryable single result this way the query can still be modified by the $select and others
            return Ok(SingleResult.Create(people));
        }


        [HttpGet]
        [ODataRoute("People({key})/Email")]
        [ODataRoute("People({key})/FirstName")]
        [ODataRoute("People({key})/LastName")]
        [ODataRoute("People({key})/DateOfBirth")]
        [ODataRoute("People({key})/Gender")]
        public IHttpActionResult GetPersonProperty([FromODataUri] int key)
        {
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            if (person == null)
            {
                return NotFound();
            }
            var propertyToGet = Url.Request.RequestUri.Segments.Last();
            if(!person.HasProperty(propertyToGet))
            {
                return NotFound();
            }
            var propertyValue = person.GetValue(propertyToGet);
            if(propertyValue == null)
            {
                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            return this.CreateOKHttpActionResult(propertyValue);
        }

        [HttpGet]
        [ODataRoute("People({key})/Email/$value")]
        [ODataRoute("People({key})/FirstName/$value")]
        [ODataRoute("People({key})/LastName/$value")]
        [ODataRoute("People({key})/DateOfBirth/$value")]
        [ODataRoute("People({key})/Gender/$value")]
        public IHttpActionResult GetPersonPropertyRawValue([FromODataUri] int key)
        {
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            if (person == null)
            {
                return NotFound();
            }
            var propertyToGet = Url.Request.RequestUri
                                   .Segments[Url.Request.RequestUri.Segments.Length - 2].TrimEnd('/');
            if (!person.HasProperty(propertyToGet))
            {
                return NotFound();
            }
            var propertyValue = person.GetValue(propertyToGet);
            if (propertyValue == null)
            {
                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            return this.CreateOKHttpActionResult(propertyValue.ToString());
        }

        public IHttpActionResult Post(Person person)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _ctx.People.Add(person);
            _ctx.SaveChanges();

            return Created(person);
        }

        public IHttpActionResult Put([FromODataUri] int key, Person person)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var currentPerson = _ctx.People.FirstOrDefault(p => p.PersonId == key);

            if(currentPerson == null)
            {
                return NotFound();
            }

            //Overwrite the persons ID with the one provided from the URI to avoid overwritting a different person
            person.PersonId = currentPerson.PersonId;

            //Update values from the person
            _ctx.Entry(currentPerson).CurrentValues.SetValues(person);
            _ctx.SaveChanges();

            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }

        public IHttpActionResult Patch([FromODataUri] int key, Delta<Person> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var currentPerson = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            if (currentPerson == null)
            {
                return NotFound();
            }

            patch.Patch(currentPerson);
            _ctx.SaveChanges();
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }

        public IHttpActionResult Delete([FromODataUri] int key)
        {
            var currentPerson = _ctx.People.Include("Friends").FirstOrDefault(p => p.PersonId == key);
            if(currentPerson == null)
            {
                return NotFound();
            }
            // Remove current person from the friends list of everyone who knew him.
            var peopleWithCurrentPersonAsFriend = _ctx.People.Include("Friends")
                .Where(p => p.Friends.Select(f => f.PersonId).AsQueryable().Contains(key));
            foreach(var person in peopleWithCurrentPersonAsFriend.ToList())
            {
                person.Friends.Remove(currentPerson);
            }

            _ctx.People.Remove(currentPerson);
            _ctx.SaveChanges();
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
        #endregion

        #region Persons Friends
        [HttpGet]
        [EnableQuery]
        [ODataRoute("People({key})/Friends")]
        public IHttpActionResult GetFriendRecordsForPerson([FromODataUri] int key)
        {
            var person = _ctx.People.Include("Friends").FirstOrDefault(p => p.PersonId == key);
            if (person == null)
            {
                return NotFound();
            }
            return Ok(person.Friends);
        }
        [HttpPost]
        [ODataRoute("People({key})/Friends/$ref")]
        public IHttpActionResult CreateLinkToFriend([FromODataUri] int key, [FromBody] Uri link)
        {
            var currentPerson = _ctx.People.Include("Friends").FirstOrDefault(p => p.PersonId == key);
            if(currentPerson == null)
            {
                return BadRequest();
            }

            int keyOfFriendToAdd = Request.GetKeyValue<int>(link);
            if(currentPerson.Friends.Any(i=>i.PersonId == keyOfFriendToAdd))
            {
                return BadRequest("Already friends.");
            }

            var friendToLinkTo = _ctx.People.FirstOrDefault(p => p.PersonId == keyOfFriendToAdd);
            if(friendToLinkTo == null)
            {
                return NotFound();
            }

            currentPerson.Friends.Add(friendToLinkTo);
            _ctx.SaveChanges();
            return StatusCode(System.Net.HttpStatusCode.NoContent);

        }
        [HttpPut]
        [ODataRoute("People({key})/Friends({relatedKey})/$ref")]
        public IHttpActionResult UpdateLinkToFriend([FromODataUri] int key, [FromODataUri] int relatedKey, [FromBody]Uri link)
        {
            var currentPerson = _ctx.People.Include("Friends").FirstOrDefault(p => p.PersonId == key);
            if (currentPerson == null)
            {
                return BadRequest();
            }

            var currentFriend = currentPerson.Friends.FirstOrDefault(p => p.PersonId == relatedKey);
            if(currentFriend == null)
            {
                return NotFound();
            }

            int keyOfFriendToAdd = Request.GetKeyValue<int>(link);
            if (currentPerson.Friends.Any(i => i.PersonId == keyOfFriendToAdd))
            {
                return BadRequest("Already friends.");
            }

            var friendToLinkTo = _ctx.People.FirstOrDefault(p => p.PersonId == keyOfFriendToAdd);
            if (friendToLinkTo == null)
            {
                return NotFound();
            }

            currentPerson.Friends.Remove(currentFriend);
            currentPerson.Friends.Add(friendToLinkTo);
            _ctx.SaveChanges();

            return StatusCode(System.Net.HttpStatusCode.NoContent);

        }

        [HttpDelete]
        [ODataRoute("People({key})/Friends({relatedKey})/$ref")]
        public IHttpActionResult DeleteLinkToFriend([FromODataUri] int key, [FromODataUri] int relatedKey)
        {
            var currentPerson = _ctx.People.Include("Friends").FirstOrDefault(p => p.PersonId == key);
            if (currentPerson == null)
            {
                return BadRequest();
            }

            var friend = currentPerson.Friends.FirstOrDefault(p => p.PersonId == relatedKey);
            if (friend == null)
            {
                return NotFound();
            }

            currentPerson.Friends.Remove(friend);
            _ctx.SaveChanges();
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
        #endregion

        #region Persons VinylRecords
        [HttpGet]
        [EnableQuery]
        [ODataRoute("People({key})/VinylRecords")]
        public IHttpActionResult GetVinylRecordsForPerson([FromODataUri] int key)
        {
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            if (person == null)
            {
                return NotFound();
            }
            return Ok(_ctx.VinylRecords.Where(v => v.Person.PersonId == key));
        }
        [HttpGet]
        [EnableQuery]
        [ODataRoute("People({key})/VinylRecords({vinylRecordKey})")]
        public IHttpActionResult GetVinylRecordForPerson([FromODataUri] int key, [FromODataUri] int vinylRecordKey)
        {
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            if (person == null)
            {
                return NotFound();
            }
            var vinylRecord = _ctx.VinylRecords.Where(v => v.Person.PersonId == key && v.VinylRecordId == vinylRecordKey);
            if (!vinylRecord.Any())
            {
                return NotFound();
            }

            return Ok(SingleResult.Create(vinylRecord));
        }

        [HttpPost]
        [ODataRoute("People({key})/VinylRecords")]
        public IHttpActionResult CreateVinylRecordForPerson([FromODataUri] int key, VinylRecord vinylRecord)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // does the person exist?
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            if(person == null)
            {
                return NotFound();
            }
            // link the person to the VinylRecord (also avoids an invalid person key on the passed-in record - key from URI wins)
            vinylRecord.Person = person;

            // add vinyl record
            _ctx.VinylRecords.Add(vinylRecord);
            _ctx.SaveChanges();
            //Add the vinyl record
            return Created(vinylRecord);
        }

        [HttpPatch]
        [ODataRoute("People({key})/VinylRecords({vinylRecordKey})")]
        public IHttpActionResult UpdateVinylRecord([FromODataUri] int key, [FromODataUri] int vinylRecordKey, Delta<VinylRecord> patch)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var person = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            if(person == null)
            {
                return NotFound();
            }

            var currentVinylRecord = _ctx.VinylRecords.FirstOrDefault(p => p.VinylRecordId == vinylRecordKey && p.Person.PersonId == key);
            if(currentVinylRecord == null)
            {
                return NotFound();
            }
            patch.Patch(currentVinylRecord);
            _ctx.SaveChanges();

            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
        [HttpDelete]
        [ODataRoute("People({key})/VinylRecords({vinylRecordKey})")]
        public IHttpActionResult DeleteVinylRecord([FromODataUri] int key, [FromODataUri] int vinylRecordKey)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var person = _ctx.People.FirstOrDefault(p => p.PersonId == key);
            if (person == null)
            {
                return NotFound();
            }

            var currentVinylRecord = _ctx.VinylRecords.FirstOrDefault(p => p.VinylRecordId == vinylRecordKey && p.Person.PersonId == key);
            if (currentVinylRecord == null)
            {
                return NotFound();
            }
            _ctx.VinylRecords.Remove(currentVinylRecord);
            _ctx.SaveChanges();

            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            _ctx.Dispose();
            base.Dispose(disposing);
        }
    }
}