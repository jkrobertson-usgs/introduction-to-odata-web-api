﻿using AirVinyl.DataAccessLayer;
using AirVinyl.Model;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;

namespace AirVinyl.API.Controllers
{
    public class RecordStoresController : ODataController
    {
        // context
        private AirVinylDbContext _ctx = new AirVinylDbContext();

        #region Record Store
        // GET odata/RecordStores
        [EnableQuery]
        public IHttpActionResult Get()
        {
            return Ok(_ctx.RecordStores);
        }     

        // GET odata/RecordStores(key)
        [EnableQuery]
        public IHttpActionResult Get([FromODataUri] int key)
        {
            var recordStores = _ctx.RecordStores.Where(p => p.RecordStoreId == key);

            if (!recordStores.Any())
            {
                return NotFound();
            }

            return Ok(SingleResult.Create(recordStores));
        }
        #endregion

        #region Specialized Record Store
        [HttpGet]
        [ODataRoute("RecordStores/AirVinyl.Model.SpecializedRecordStore")]
        [EnableQuery]
        public IHttpActionResult GetSpecializedStores()
        {
            var specializedStores = _ctx.RecordStores.Where(r => r is SpecializedRecordStore);
            return Ok(specializedStores.Select(s => s as SpecializedRecordStore));
        }
        [HttpGet]
        [ODataRoute("RecordStores({key})/AirVinyl.Model.SpecializedRecordStore")]
        public IHttpActionResult GetSpecializedStore([FromODataUri] int key)
        {
            var specializedStores = _ctx.RecordStores.Where(r => r is SpecializedRecordStore
                                                               && r.RecordStoreId == key);
            if(!specializedStores.Any())
            {
                return NotFound();
            }

            return Ok(specializedStores.Single());
        }
        #endregion

        #region Record Store Tags
        [HttpGet]
        [ODataRoute("RecordStores({key})/Tags")]
        [EnableQuery]
        public IHttpActionResult GetRecordStoreTagsProperty([FromODataUri] int key)
        {
            // no Include necessary for EF - Tags isn't a navigation property 
            // in the entity model.  
            var recordStore = _ctx.RecordStores
                .FirstOrDefault(p => p.RecordStoreId == key);

            if (recordStore == null)
            {
                return NotFound();
            }

            var collectionPropertyToGet = Url.Request.RequestUri.Segments.Last();
            var collectionPropertyValue = recordStore.GetValue(collectionPropertyToGet);

            // return the collection of tags
            return this.CreateOKHttpActionResult(collectionPropertyValue);
        }
        #endregion

        #region Functions
        [HttpGet]
        [ODataRoute("RecordStores({key})/AirVinyl.Functions.IsHighRated(minimumRating={minimumRating})")]
        public bool IsHighRated([FromODataUri] int key, int minimumRating)
        {
            var recordStore = _ctx.RecordStores.FirstOrDefault(p => p.RecordStoreId == key && p.Ratings.Any() && (p.Ratings.Sum(r => r.Value) / p.Ratings.Count) >= minimumRating);
            return (recordStore != null);
        }
        [HttpGet]
        [ODataRoute("RecordStores/AirVinyl.Functions.AreRatedBy(personIds={personIds})")]
        public IHttpActionResult AreRatedBy([FromODataUri] IEnumerable<int> personIds)
        {
            // get the record stores
            var recordStores = _ctx.RecordStores
                                   .Where(p => p.Ratings.Any(r => personIds.Contains(r.RatedBy.PersonId)));
            return this.CreateOKHttpActionResult(recordStores);
        }
        [HttpGet]
        [ODataRoute("GetHighRatedRecordStores(minimumRating={minimumRating})")]
        public IHttpActionResult GetHighRatedRecordStores([FromODataUri] int minimumRating)
        {
            // get the record stores
            var recordStores = _ctx.RecordStores
                                   .Where(p => p.Ratings.Any()
                                   && (p.Ratings.Sum(r => r.Value) / p.Ratings.Count) >= minimumRating);
            return this.CreateOKHttpActionResult(recordStores);
        }
        #endregion

        #region Actions
        [HttpPost]
        [ODataRoute("RecordStores({key})/AirVinyl.Actions.Rate")]
        public IHttpActionResult Rate([FromODataUri] int key, ODataActionParameters parameters)
        {
            // check that the record store exists
            var recordStore = _ctx.RecordStores.FirstOrDefault(p => p.RecordStoreId == key);
            if(recordStore == null)
            {
                return NotFound();
            }

            // from the param dictionary, get the rating and the personid
            int rating;
            int personId;
            object outputFromDictionary;
            
            if(!parameters.TryGetValue("rating", out outputFromDictionary))
            {
                return NotFound();
            }
            if(!int.TryParse(outputFromDictionary.ToString(), out rating))
            {
                return NotFound();
            }
            if (!parameters.TryGetValue("personId", out outputFromDictionary))
            {
                return NotFound();
            }
            if (!int.TryParse(outputFromDictionary.ToString(), out personId))
            {
                return NotFound();
            }

            //check that the person exists
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == personId);
            if(person == null)
            {
                return NotFound();
            }

            //Everything checks out, add the rating
            recordStore.Ratings.Add(new Model.Rating() { RatedBy = person, Value = rating });
            //save changes
            if(_ctx.SaveChanges() > -1)
            {
                return this.CreateOKHttpActionResult(true);
            }else
            {
                // Something went wrong - we expect our action to return false in that case
                // The request is still successful, false is a valid response. 
                return this.CreateOKHttpActionResult(false);
            }
        }

        [HttpPost]
        [ODataRoute("RecordStores/AirVinyl.Actions.RemoveRatings")]
        public IHttpActionResult RemoveRatings(ODataActionParameters parameters)
        {
            // from the param dictionary, get the person id
            int personId;
            object outputFromDictionary;
            if(!parameters.TryGetValue("personId", out outputFromDictionary))
            {
                return NotFound();
            }
            if(!int.TryParse(outputFromDictionary.ToString(), out personId))
            {
                return NotFound();
            }

            //Get the record stores that were rated by that person
            var recordStoresRatedByCurrentPerson = _ctx.RecordStores
                                                       .Include("Ratings").Include("Ratings.RatedBy")
                                                       .Where(p => p.Ratings.Any(r => r.RatedBy.PersonId == personId)).ToList();
            //Remove those ratings
            foreach(var store in recordStoresRatedByCurrentPerson)
            {
                var ratingByCurrentPerson = store.Ratings.Where(r => r.RatedBy.PersonId == personId).ToList();
                for(int i = 0; i < ratingByCurrentPerson.Count(); i++)
                {
                    store.Ratings.Remove(ratingByCurrentPerson[i]);
                }
            }

            //Save Changes
            if(_ctx.SaveChanges() > -1)
            {
                return this.CreateOKHttpActionResult(true);
            }else
            {
                return this.CreateOKHttpActionResult(false);
            }
        }

        [HttpPost]
        [ODataRoute("RemoveRecordStoreRatings")]
        public IHttpActionResult RemoveRecordStoreRatings(ODataActionParameters parameters)
        {
            // from the param dictionary, get the person id
            int personId;
            object outputFromDictionary;
            if (!parameters.TryGetValue("personId", out outputFromDictionary))
            {
                return NotFound();
            }
            if (!int.TryParse(outputFromDictionary.ToString(), out personId))
            {
                return NotFound();
            }

            //Get the record stores that were rated by that person
            var recordStoresRatedByCurrentPerson = _ctx.RecordStores
                                                       .Include("Ratings").Include("Ratings.RatedBy")
                                                       .Where(p => p.Ratings.Any(r => r.RatedBy.PersonId == personId)).ToList();
            //Remove those ratings
            foreach (var store in recordStoresRatedByCurrentPerson)
            {
                var ratingByCurrentPerson = store.Ratings.Where(r => r.RatedBy.PersonId == personId).ToList();
                for (int i = 0; i < ratingByCurrentPerson.Count(); i++)
                {
                    store.Ratings.Remove(ratingByCurrentPerson[i]);
                }
            }

            //Save Changes
            if (_ctx.SaveChanges() > -1)
            {
                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            else
            {
                return StatusCode(System.Net.HttpStatusCode.InternalServerError);
            }
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            // dispose the context
            _ctx.Dispose();
            base.Dispose(disposing);
        }
    }
}
