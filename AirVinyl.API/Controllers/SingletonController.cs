﻿using AirVinyl.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;

namespace AirVinyl.API.Controllers
{
    public class SingletonController : ODataController
    {
        private AirVinylDbContext _ctx = new AirVinylDbContext();

        #region Tim
        [HttpGet]
        [ODataRoute("Tim")]
        public IHttpActionResult GetSingletonTim()
        {
            //Get tim, his id is 6
            var personTim = _ctx.People.FirstOrDefault(p => p.PersonId == 6);

            return Ok(personTim);
        }
        [HttpGet]
        [ODataRoute("Tim/Email")]
        [ODataRoute("Tim/FirstName")]
        [ODataRoute("Tim/LastName")]
        [ODataRoute("Tim/DateOfBirth")]
        [ODataRoute("Tim/Gender")]
        public IHttpActionResult GetTimsProperty()
        {
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == 6);
            if (person == null)
            {
                return NotFound();
            }
            var propertyToGet = Url.Request.RequestUri.Segments.Last();
            if (!person.HasProperty(propertyToGet))
            {
                return NotFound();
            }
            var propertyValue = person.GetValue(propertyToGet);
            if (propertyValue == null)
            {
                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            return this.CreateOKHttpActionResult(propertyValue);
        }

        [HttpGet]
        [ODataRoute("Tim/Email/$value")]
        [ODataRoute("Tim/FirstName/$value")]
        [ODataRoute("Tim/LastName/$value")]
        [ODataRoute("Tim/DateOfBirth/$value")]
        [ODataRoute("Tim/Gender/$value")]
        public IHttpActionResult GetTimsPropertyRawValue()
        {
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == 6);
            if (person == null)
            {
                return NotFound();
            }
            var propertyToGet = Url.Request.RequestUri
                                   .Segments[Url.Request.RequestUri.Segments.Length - 2].TrimEnd('/');
            if (!person.HasProperty(propertyToGet))
            {
                return NotFound();
            }
            var propertyValue = person.GetValue(propertyToGet);
            if (propertyValue == null)
            {
                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            return this.CreateOKHttpActionResult(propertyValue.ToString());
        }
        [HttpGet]
        [EnableQuery]
        [ODataRoute("Tim/Friends")]
        public IHttpActionResult GetTimsRecordsForPerson()
        {
            var person = _ctx.People.Include("Friends").FirstOrDefault(p => p.PersonId == 6);
            if (person == null)
            {
                return NotFound();
            }
            return Ok(person.Friends);
        }

        [HttpGet]
        [EnableQuery]
        [ODataRoute("Tim/VinylRecords")]
        public IHttpActionResult GetVinylRecordsForTim()
        {
            var person = _ctx.People.FirstOrDefault(p => p.PersonId == 6);
            if (person == null)
            {
                return NotFound();
            }
            return Ok(_ctx.VinylRecords.Where(v => v.Person.PersonId == 6));
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            _ctx.Dispose();
            base.Dispose(disposing);
        }
    }
}